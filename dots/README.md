CloverOS Dotfiles
-----------------------------
Here lies my submissions for default cloverOS WM/DE configurations.  

## i3-gaps
-----------------------------
![i3-gaps](https://github.com/TheNightmanCodeth/cloveros/raw/master/dots/i3/scrot_cos.png "Clover i3")

Config goes in .config/i3/  
Scripts go in /usr/bin  
compton.config goes in .config/

Wallpaper can be changed with random images sourced from ~/Pictures/Wallpapers. The titlebar and terminal colors change with the wallpaper using [wal](https://github.com/dylanaraps/wal). The operation is binded to ($Mod+Shift+w). i3 modifier is win by default.
